import { Router } from 'express';
import * as db from '../db/sportpalyadb.js';

const router = Router();

// felhasznalo regisztralasa
// regisztracios formba beirt adatok feldolgozasa
router.post('/felhasznaloRegisztracio', async (req, resp) => {
  const nev = req.fields.teljesNev;
  const felhasznalonev = req.fields.felhasznaloNev;
  const sajatJelszo = req.fields.jelszo1;
  const jelszoIsmetlese = req.fields.jelszo2;
  let hibakod = 0;

  // ha a szemely nem irt be jelszot, akkor jelezzuk, hogy jelszo megadasa kotelezo
  if (sajatJelszo.length === 0) {
    hibakod = 1;
    resp.redirect(`/regisztracio${hibakod}`);
  }

  // ha valamely mezo ures, jelezzuk, hogy minden mezo kitoltese kotelezo
  if (nev.length === 0 || felhasznalonev.length === 0 || jelszoIsmetlese.length === 0) {
    hibakod = 4;
    resp.redirect(`/regisztracio${hibakod}`);
  }

  // ha a beirt ket jelszo nem egyezik meg jelezzuk ezt a szemelynek
  if (sajatJelszo !== jelszoIsmetlese) {
    hibakod = 2;
    resp.redirect(`/regisztracio${hibakod}`);
  }

  // megvizsgaljuk, hogy letezik-e mar a rendszerben a megadott felhasznalonev
  const letezikRegisztralt = await db.countFelhasznalonev(felhasznalonev);
  const letezikVarolista = await db.countVarolista(felhasznalonev);
  if (letezikRegisztralt[0].darab !== 0 && letezikVarolista[0].darab !== 0) {
    hibakod = 3;
    resp.redirect(`/regisztracio${hibakod}`);
  } else {
    // ha minden rendben, akkor a felhasznalot az admin altal karbantartott varolistara tesszuk
    const varolista = {
      nev,
      felhasznalonev,
      jelszo: sajatJelszo,
    };
    await db.insertVarolista(varolista);
    resp.redirect('/bejelentkezes');
  }
});

export default router;
