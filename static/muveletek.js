async function foglalasokListazasa(id, szemID) {
  const valasz = await fetch(`/api/foglalasokMegtekintese/${id}`);
  const eredmeny = await valasz.json();
  if (valasz.status < 400) {
    let uzenet = '';
    let tag;
    let gomb;
    let elemek = 0;
    let alkotoID;
    eredmeny.forEach((foglalas) => {
      elemek = 1;
      alkotoID = foglalas.szemID;
      if (Number(szemID) === Number(alkotoID)) {
        gomb = ` <input class="torles" onclick="foglalasTorlese(event, ${foglalas.foglalasID})" type="button" value="Foglalás törlése">`;
        tag = ` <div id="foglalas${foglalas.foglalasID}"> <p> Foglaló neve: ${foglalas.nev} <br> Foglalás időtartama:  ${foglalas.kezdDatum} - ${foglalas.vegDatum} ${gomb} </p> <br> </div>`;
      } else {
        tag = ` <div id="foglalas${foglalas.foglalasID}"> <p> Foglaló neve: ${foglalas.nev} <br> Foglalás időtartama:  ${foglalas.kezdDatum} - ${foglalas.vegDatum} </p> <br> </div>`;
      }
      uzenet = `${uzenet} ${tag}`;
    });
    if (elemek === 1) {
      document.getElementById(`foglalasok${id}`).innerHTML = uzenet;
    } else {
      uzenet = ' <p> Az adott sportpálya esetén nincs egy megjeleníthető foglalás sem. </p>';
      document.getElementById(`foglalasok${id}`).innerHTML = uzenet;
    }
  }
}

function szabadHelyek(id, szemID) {
  const date = new Date();
  const n = date.getDay();
  let uzenet = '';
  if (n <= 1) {
    const hetfo = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 1, ${szemID})" type="button" value="hétfő"> `;
    uzenet = `${uzenet} ${hetfo}`;
  }
  if (n <= 2) {
    const kedd = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 2, ${szemID})" type="button" value="kedd">`;
    uzenet = `${uzenet} ${kedd}`;
  }
  if (n <= 3) {
    const szerda = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 3, ${szemID})" type="button" value="szerda">`;
    uzenet = `${uzenet} ${szerda}`;
  }
  if (n <= 4) {
    const csut = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 4, ${szemID})" type="button" value="csütörtök">`;
    uzenet = `${uzenet} ${csut}`;
  }
  if (n <= 5) {
    const pentek = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 5, ${szemID})" type="button" value="péntek">`;
    uzenet = `${uzenet} ${pentek}`;
  }
  if (n <= 6) {
    const szombat = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 6, ${szemID})" type="button" value="szombat">`;
    uzenet = `${uzenet} ${szombat}`;
  }
  if (n <= 7) {
    const vasarnap = `<input class="button" onclick="szabadHelyekMegtekintese(event, ${id}, 7, ${szemID})" type="button" value="vasárnap">`;
    uzenet = `${uzenet} ${vasarnap}`;
  }
  document.getElementById(`szabadHelyek${id}`).innerHTML = uzenet;
}

function foglaltDatum(datum, datumKezd, datumVeg) {
  const sajatDatum = new Date(datum);
  const kezdDatum = new Date(datumKezd);
  const vegDatum = new Date(datumVeg);
  console.log(` 1: ${sajatDatum}`);
  console.log(`2: ${kezdDatum}`);
  console.log(`3: ${vegDatum}`);

  if (sajatDatum >= kezdDatum && sajatDatum <= vegDatum) {
    console.log('Itt2');
    return true;
  }
  return false;
}

function kozrezar(datum1, datum2, datumKezd, datumVeg) {
  const sajatDatumKezd = new Date(datum1);
  const sajatDatumVeg = new Date(datum2);
  const kezdDatum = new Date(datumKezd);
  const vegDatum = new Date(datumVeg);
  if (sajatDatumKezd <= kezdDatum && sajatDatumVeg >= vegDatum) {
    console.log('Itt1');
    return true;
  }
  return false;
}

async function helyetFoglal(event, id, ora, napSzam) {
  const valasz = await fetch(`api/lefoglalas/${id}/${ora}/${napSzam}`);
  if (valasz.status < 400) {
    event.target.parentNode.remove();
  } else {
    const hiba = await valasz.json();
    console.log(hiba);
  }
}

async function szabadHelyekMegtekintese(event, id, napSzam, szemID) {
  const szabad = await fetch(`/api/foglalasokLekerese/${id}`);
  const eredmeny = await szabad.json();
  const foglalasok = [];
  eredmeny.forEach((foglalas) => {
    const datum1 = foglalas.kezdDatum;
    const datum2 = foglalas.vegDatum;
    foglalasok.push({ datum1, datum2 });
  });
  const date = new Date();
  const n = date.getDay();
  let ora;
  let dateKezd;
  let dateVeg;
  let ev;
  let honap;
  let nap;
  let uzenet = '';
  console.log(`${napSzam} ${n}`);
  if (napSzam === n) {
    ora = date.getHours() + 1;
  } else {
    ora = 8;
  }
  for (; ora <= 21; ora += 1) {
    ev = date.getFullYear();
    honap =  date.getMonth();
    nap = date.getDate() + napSzam - n;
    dateKezd = new Date(ev, honap, nap, ora, 0, 0);
    dateKezd = dateKezd.toISOString();
    dateVeg = new Date(ev, honap, nap, ora + 1, 0, 0);
    dateVeg = dateVeg.toISOString();
    let foglalt = false;
    let i;
    for (i = 0; i < foglalasok.length; i += 1) {
      console.log(foglalasok[i]);
      const { datum1 } = foglalasok[i];
      const { datum2 } = foglalasok[i];
      console.log(`Lassuk: ${datum1} ${datum2}`);
      const foglalte = foglaltDatum(dateKezd, datum1, datum2);
      const foglalalte2 = foglaltDatum(dateVeg, datum1, datum2);
      if (foglalte === true || foglalalte2 === true) {
        foglalt = true;
      }
      if (kozrezar(dateKezd, dateVeg, foglalasok[i]) === true) {
        foglalt = true;
      }
    }
    if (foglalt === false) {
      const szoveg = `<div class="egyseg"> Dátum: ${ev}.${honap + 1}.${nap} <br> Időpont: ${ora} - ${ora + 1} <br><input class="button" onclick="helyetFoglal(event, ${id}, ${ora}, ${napSzam})" type="button" value="Lefoglal"><br><br></div>`;
      uzenet = `${uzenet} ${szoveg}`;
    }
  }
  document.getElementById(`szabadHelyekLista${id}`).innerHTML = uzenet;
}

async function foglalasTorlese(event, id) {
  const valasz = await fetch(`/api/foglalasTorlese/${id}`, {
    method: 'DELETE',
  });
  if (valasz.status < 400) {
    event.target.parentNode.remove();
  } else {
    const hiba = await valasz.json();
    console.log(hiba);
  }
}

async function felhasznaloTorlese(event, id) {
  const valasz = await fetch(`/api/felhasznaloTorlese/${id}`, {
    method: 'DELETE',
  });
  if (valasz.status < 400) {
    event.target.parentNode.remove();
  } else {
    const hiba = await valasz.json();
    console.log(hiba);
  }
}

async function kepTorlese(event, nev) {
  const szetbont = nev.split('/');
  const valasz = await fetch(`/api/kepTorlese/${szetbont[1]}`, {
    method: 'DELETE',
  });
  if (valasz.status < 400) {
    event.target.parentNode.remove();
  } else {
    const hiba = await valasz.json();
    console.log(hiba);
  }
}

async function felhasznaloTorleseAdmin(event, id) {
  const valasz = await fetch(`/api/varolistaTorlese/${id}`, {
    method: 'DELETE',
  });
  if (valasz.status < 400) {
    event.target.parentNode.remove();
  } else {
    const hiba = await valasz.json();
    console.log(hiba);
  }
}

async function felhasznaloEngedelyezese(event, id) {
  const valasz = await fetch(`/api/felhasznaloEngedelyezese/${id}`, {
    method: 'DELETE',
  });
  if (valasz.status < 400) {
    event.target.parentNode.remove();
  } else {
    const hiba = await valasz.json();
    console.log(hiba);
  }
}
