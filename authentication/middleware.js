import jwt from 'jsonwebtoken';
import secret from './secret.js';

export function tokenDekodolasa(req, res, next) {
  const { token } = req.cookies;
  if (token) {
    try {
      const tokenObj = jwt.verify(token, secret);
      res.locals.tokenObj = tokenObj;
    } catch (error) {
      res.clearCookie('token');
    }
  }
  next();
}

export function tokenEllenorzese(req, resp, next) {
  if (resp.locals.tokenObj) {
    next();
  } else {
    resp.status(401);
    resp.render('bejelentkezes');
  }
}
