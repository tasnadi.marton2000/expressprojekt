import { Router } from 'express';
import jwt from 'jsonwebtoken';
import secret from './secret.js';
import * as db from '../db/sportpalyadb.js';

const router = Router();

// bejelentkezes eseten ellenorzi, hogy a felhasznalo letezik-e, illetve megfelelo adatokat irt be
// ha megfelelo, akkor egy cookie-ban megorzi az adatait
router.post('/bejelentkezes', async (req, resp) => {
  const felhasznalonev = req.fields.felhasznaloNev;
  const sajatJelszo = req.fields.jelszo;
  let hibakod = 0;

  // megvizsgaljuk, hogy bejelentkezeskor minden mezot kitoltott-e a felhasznalo
  if (felhasznalonev.length === 0 || sajatJelszo.length === 0) {
    hibakod = 3;
    resp.redirect(`/bejelentkezes${hibakod}`);
  }

  // megvizsgaljuk, hogy a beirt felhasznalonevvel van-e regisztralt felhasznalo a rendszerben
  const szerepel = await db.countFelhasznalonev(felhasznalonev);
  if (szerepel[0].darab === 0) {
    hibakod = 1;
    resp.redirect(`/bejelentkezes${hibakod}`);
  } else {
    const adatok = await db.selectAdatok(felhasznalonev);
    const jelszoSzoveg = adatok[0].jelszo;

    // ha van, megvizsgaljuk, hogy helyes jelszot irt-e be
    if (sajatJelszo === jelszoSzoveg) {
      const token = jwt.sign({
        felhasznaloNev: felhasznalonev,
        jogosultsag: adatok[0].jogosultsag,
        szemID: adatok[0].szemID,
        nev: adatok[0].nev,
      }, secret, {
        expiresIn: '1h',
      });
      resp.cookie('token', token, {
        httpOnly: true,
        sameSite: 'strict',
      });
      resp.redirect('/');
    } else {
      hibakod = 2;
      resp.redirect(`/bejelentkezes${hibakod}`);
    }
  }
});

// felhasznalo kijelentkezese, mely soran visszterunk a fooldalra
router.post('/kijelentkezes', (req, resp) => {
  resp.clearCookie('token');
  resp.redirect('/');
});

export default router;
