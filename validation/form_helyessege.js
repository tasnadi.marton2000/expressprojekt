export function foglalt(foglalasKezd, foglalasVeg, eddigiFoglalas) {
  let szabad = 1;
  for (let i = 0; i < eddigiFoglalas.length; i += 1) {
    if (foglalasKezd > eddigiFoglalas[i].kezdDatum && foglalasKezd < eddigiFoglalas[i].vegDatum) {
      szabad = 0;
    }
    if (foglalasVeg > eddigiFoglalas[i].kezdDatum && foglalasVeg < eddigiFoglalas[i].vegDatum) {
      szabad = 0;
    }
  }
  return szabad;
}

export function datumCheck(idopont, datum) {
  if (!/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]$/.test(datum)) {
    return 0;
  }

  const szetbont = datum.split('-');
  const ev = szetbont[0];
  const ho = szetbont[1];
  const szetbont1 = szetbont[2].split('T');
  const nap = szetbont1[0];
  const szetbont2 = szetbont1[1].split(':');
  const ora = szetbont2[0];
  const perc = szetbont2[1];

  if (idopont.getUTCFullYear() !== Number(ev)) {
    return 0;
  }

  if (idopont.getUTCMonth() + 1 !== Number(ho)) {
    return 0;
  }

  if (idopont.getUTCDate() !== Number(nap)) {
    return 0;
  }

  if (idopont.getUTCHours() !== Number(ora)) {
    return 0;
  }

  if (idopont.getUTCMinutes() !== Number(perc)) {
    return 0;
  }
  return 1;
}
