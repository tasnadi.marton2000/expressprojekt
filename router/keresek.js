import express from 'express';
import eformidable from 'express-formidable';
import { join } from 'path';
import * as db from '../db/sportpalyadb.js';
import { tokenEllenorzese } from '../authentication/middleware.js';
import {
  foglalt,
  datumCheck,
} from '../validation/form_helyessege.js';

const router = express.Router();
const uploadDir = join(process.cwd(), 'static/uploadDir');

// uj sportpalya bevezetese a rendszerbe
router.post('/sportpalya_bevezetese', tokenEllenorzese, async (request, response) => {
  try {
    if (!/^[A-Za-z]+$/.test(String(request.body.tipus))) {
      return response.status(400).send('Helytelen a típus formatuma.');
    }

    if (!/^[0-9]+$/.test(request.body.oraber)) {
      return response.status(400).send('Helytelen formátumú órabér. Az órabér csak számjegyekből állhat.');
    }
    const x = request.body;
    const szemSzam = response.locals.tokenObj.szemID;
    const palya = {
      tipus: x.tipus, oraber: x.oraber, cim: x.cim, leiras: x.leiras, szemID: szemSzam,
    };

    await db.insertPalya(palya);
    return response.redirect('/');
  } catch (error) {
    console.log(`Hiba: ${error}`);
    response.set('Content-Type', 'text/plain;charset=utf-8');
    return response.end('Sikertelen beszúrás!');
  }
});

router.get('/sportpalya', tokenEllenorzese, async (req, resp) => {
  try {
    return resp.render('sportpalya');
  } catch (error) {
    return resp.status(500).send('Hiba lépett fel a sportpályák bevezetése oldálának betöltésekor.');
  }
});

// sportpalya foglalasa
router.post('/sportpalya_foglalasa', tokenEllenorzese, async (request, response) => {
  const szemSzam = response.locals.tokenObj.szemID;
  const palyaAzonosito = request.body.palya;
  const foglalasKezdete = request.body.kezdDatum;
  const foglalasVege = request.body.vegDatum;

  const idopont1 = new Date(`${foglalasKezdete}Z`);
  const idopont2 = new Date(`${foglalasVege}Z`);
  let uzenet;

  // datumok formatumanak helyessegenek ellenorzese
  if (datumCheck(idopont1, foglalasKezdete) === 0 || datumCheck(idopont2, foglalasVege) === 0) {
    uzenet = 2;
    return response.redirect(`foglalas${uzenet}`);
  }

  // ha a foglalas vege hamarabb van, mint a foglalas kezdete
  if (idopont1 > idopont2) {
    uzenet = 3;
    return response.redirect(`foglalas${uzenet}`);
  }

  const eddigiFoglalasok = await db.foglalasokIdopontja(palyaAzonosito);
  const szabad = foglalt(idopont1, idopont2, eddigiFoglalasok);

  // annak ellenorzese, hogy a megadott idointervallumban az adott palya szabad-e
  response.set('Content-Type', 'text/plain;charset=utf-8');
  if (szabad) {
    const objektum = {
      szemID: szemSzam,
      ID: palyaAzonosito,
      kezdDatum: idopont1,
      vegDatum: idopont2,
    };
    await db.insertFoglalas(objektum);
    uzenet = 0;
    return response.redirect(`foglalas${uzenet}`);
  }
  uzenet = 1;
  return response.redirect(`foglalas${uzenet}`);
});

// foglalasi form megjelenitese abban az esetben, ha valamilyen hibauzenetet kell megjelenitsunk
router.get('/foglalas?:uzenet', tokenEllenorzese,  async (req, resp) => {
  try {
    const [nevek, sportpalya] = await Promise.all([db.selectNevek(), db.selectPalyak()]);
    const { uzenet } = req.params;
    return resp.render('foglalas.ejs', { nevek, sportpalya, uzenet });
  } catch (error) {
    console.log(`Hiba: ${error}`);
    return resp.status(500).send('Hiba lepett fel az adatok elkuldesekor.');
  }
});

// foglalas oldal megjelenitese, ugy hogy kiirjuk mi a foglalalo nevet es a portpalyak azonositoit
// jelen esetben nem kell kiirni hibauzenetet
router.get('/foglalas', tokenEllenorzese, async (req, resp) => {
  try {
    const [nevek, sportpalya] = await Promise.all([db.selectNevek(), db.selectPalyak()]);
    const uzenet = '';
    return resp.render('foglalas.ejs', { nevek, sportpalya, uzenet });
  } catch (error) {
    console.log(`Hiba: ${error}`);
    return resp.status(500).send('Hiba lepett fel az adatok elkuldesekor.');
  }
});

// egy adott palyarol megjelenitunk minden informaciot es a hozza tartozo kepeket is
router.get('/palyareszletek?:id', async (req, resp) => {
  try {
    const { id } = req.params;
    const [inf, kepek] = await Promise.all([db.selectPalyaID(id), db.selectKepID(id)]);
    const nevek = [];
    kepek.forEach((element) => {
      const utvonal = `uploadDir/${element.kepnev}`;
      nevek.push(utvonal);
    });
    return resp.render('palyareszletek', { inf, nevek });
  } catch (error) {
    console.log(error);
    return resp.status(500).send('Hiba lepett fel a pályarészletek megjelenítésénél.');
  }
});

// bejelentkezesi oldal megjelenitese abban az esetben, ha hibauzenetet is meg kell jeleniteni
router.get('/bejelentkezes?:hibakod', (req, resp) => {
  const { hibakod } = req.params;
  resp.render('bejelentkezes', { hibakod });
});

// bejelentkezesi oldal megjelenitese hibauzenet nelkul
router.get('/bejelentkezes', (req, resp) => {
  resp.render('bejelentkezes');
});

router.post('/bejelentkezes', (req, resp) => {
  resp.render('bejelentkezes');
});

// keresesi muvelet vegrehajtasa
// megkeressuk azokat a palyakat, amelyek tipusanak neve tartalmazza a beirt karakterlancot
router.post('/kereses', async (req, resp) => {
  try {
    const szo = req.body.keres;
    const palyak = await db.palyaKeresese(szo);
    resp.render('index', { palyak });
  } catch (err) {
    console.log(`Hiba: ${err}`);
    resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
  }
});

// szures
// megjelenitjuk azokat a palyakat, amelyek nem foglaltak a megadott datumon az adott idopontban
router.post('/szures', async (req, resp) => {
  try {
    const datum = req.body.szuresDatum;
    const ido = req.body.szuresIdo;
    const szetbontDatum = datum.split('-');
    const szetbontIdo = ido.split(':');
    const ido1 = szetbontIdo[0];
    const ido2 = szetbontIdo[1];
    const datum1 = szetbontDatum[0];
    const datum2 = szetbontDatum[1];
    const datum3 = szetbontDatum[2];
    const vegsoDatum = new Date(datum1, datum2, datum3, ido1, ido2, 0, 0);
    let palyak = await db.palyaSzurese(vegsoDatum);
    console.log(palyak);
    const lista = [];
    let i;
    const palyakAdatai = await db.palyakLekerese();
    for (i = 0; i < palyak.length; i += 1) {
      let j = 0;
      for (j = 0; j < palyakAdatai.length; j += 1) {
        if (palyakAdatai[j].ID === palyak[i].ID) {
          lista.push(palyakAdatai[i]);
        }
      }
    }
    palyak = lista;
    console.log(palyak);
    resp.render('index', { palyak });
  } catch (err) {
    console.log(`Hiba: ${err}`);
    resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
  }
});

// megjelenitjuk a foglalasokat az admin szamara
router.get('/adminFoglalasok', async (req, resp) => {
  if (resp.locals.tokenObj.jogosultsag === 0) {
    try {
      const foglalasok = await db.foglalasokLekerese();
      resp.render('adminFoglalasok', { foglalasok });
    } catch (err) {
      console.log(`Hiba: ${err}`);
      resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
    }
  } else {
    resp.status(401).send('Az adott műveletet csak az adminisztrátor hajthatja végre.');
  }
});

// listazzuk a felhasznalokat az admin szamara
router.get('/adminFelhasznalok', async (req, resp) => {
  if (resp.locals.tokenObj.jogosultsag === 0) {
    try {
      const felhasznalok = await db.felhasznalokLekerese();
      resp.render('adminFelhasznalok', { felhasznalok });
    } catch (err) {
      console.log(`Hiba: ${err}`);
      resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
    }
  } else {
    resp.status(401).send('Az adott műveletet csak az adminisztrátor hajthatja végre.');
  }
});

// lehetove tesszuk a felhasznalo szamara, hogy sajat regisztracios adatait modositani tudja
router.post('/adatokModositasa',  async (req, resp) => {
  const { szemID } = resp.locals.tokenObj;
  let { felhasznaloNev } = req.body;
  let { teljesNev } = req.body;
  const { regiJelszo } = req.body;
  let { ujJelszo } = req.body;

  // megvizsgajuk, hogy beirta-e a regi jelszavat
  if (regiJelszo.length === 0) {
    return resp.redirect('/adatokModositasa1');
  }

  // megvizsgaljuk, hogy a beirt jelszo megegyezik-e az eddigi jelszoval
  const adatok = await db.szemelyAdatok(szemID);
  console.log(adatok);
  const jelszoSzoveg = adatok.jelszo;
  if (regiJelszo !== jelszoSzoveg) {
    return resp.redirect('/adatokModositasa2');
  }

  // megvizsgaljuk, hogy az adott felhasznalonev letezik-e
  const szerepel = await db.countFelhasznalonev(felhasznaloNev);
  if (adatok.felhasznalonev !== felhasznaloNev && szerepel[0].darab !== 1) {
    return resp.redirect('/adatokModositasa3');
  }

  if (teljesNev === '') {
    teljesNev = adatok[0].nev;
  }
  if (felhasznaloNev === '') {
    felhasznaloNev = adatok[0].felhasznaloNev;
  }
  if (ujJelszo === '') {
    ujJelszo = regiJelszo;
  }
  await db.adatokatModosit(szemID, teljesNev, felhasznaloNev, ujJelszo);
  return resp.redirect('/');
});

// megjelenitjuk az oldalt, amelyek a felhasznalo modosithatja az adatait
router.get('/adatokModositasa',  async (req, resp) => {
  const szemelyID = resp.locals.tokenObj.szemID;
  const adatok = await db.szemelyAdatok(szemelyID);
  resp.render('adatokModositasa', { adatok });
});

// megjelenitjuk az oldalt, amelyek a felhasznalo modosithatja az adatait a szukseges hibauzenettel
router.get('/adatokModositasa?:hibakod',  async (req, resp) => {
  const szemelyID = resp.locals.tokenObj.szemID;
  const { hibakod } = req.params;
  console.log(hibakod);
  const adatok = await db.szemelyAdatok(szemelyID);
  resp.render('adatokModositasa', { adatok, hibakod });
});

// a sikeres regisztraciohoz az admin el kell fogadja a beirt adatokat
router.get('/regisztracioElfogadasa', async (req, resp) => {
  if (resp.locals.tokenObj.jogosultsag === 0) {
    try {
      const felhasznalok = await db.varolistaLekerese();
      return resp.render('regisztracioElfogadasa', { felhasznalok });
    } catch (err) {
      console.log(`Hiba: ${err}`);
      return resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
    }
  } else {
    return resp.status(401).send('Az adott műveletet csak az adminisztrátor hajthatja végre.');
  }
});

// bejelentkezesi oldal megjelenitese
router.get('/bejelentkezes', (req, resp) => {
  resp.render('bejelentkezes');
});

// kilepes megvalositasa
router.post('/kilepes', (req, resp) => {
  resp.render('./authentication/kijelentkezes');
});

// regisztracios oldal megjelenitese az adott hibauzenettel
router.get('/regisztracio?:hibakod', (req, resp) => {
  const { hibakod } = req.params;
  resp.render('regisztracio', { hibakod });
});

router.get('/regisztracio', (req, resp) => {
  resp.render('regisztracio');
});

// fooldal megjelenitese
router.get('/', async (req, resp) => {
  try {
    const palyak = await db.palyakLekerese();
    return resp.render('index', { palyak });
  } catch (err) {
    console.log(`Hiba: ${err}`);
    return resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
  }
});

router.get('/index', async (req, resp) => {
  try {
    const palyak = await db.palyakLekerese();
    return resp.render('index', { palyak });
  } catch (err) {
    console.log(`Hiba: ${err}`);
    return resp.status(500).send('Hiba lepett fel a lekerdezesnel.');
  }
});

// kepfeltoltesi oldal megjelenitese
router.use(eformidable({ uploadDir }));
router.post('/kepfeltoltese?:id', tokenEllenorzese, async (req, resp) => {
  const { id } = req.params;
  const szemID = await db.selectPalyaSzemID(id);
  console.log(szemID);
  if (resp.locals.tokenObj.szemID  === szemID.szemID || resp.locals.tokenObj.jogosultsag === 0) {
    const fileHandler = req.files.kep;
    if (!fileHandler.type.toString().includes('image/')) {
      return resp.status(400).send('A feltöltött file nem kép.');
    }
    let utvonal = req.files.kep.path;
    const szetbont = utvonal.split('\\');
    const hossz = szetbont.length;
    utvonal = `${szetbont[hossz - 1]}`;
    const kep = { ID: parseInt(id, 10), kepnev: utvonal };
    await db.insertKep(kep);
    return resp.redirect(`palyareszletek${req.params.id}`);
  }
  return resp.status(401).send('Az adott felhasználónak nincs jogosultsága képfeltöltésre.');
});

export default router;
