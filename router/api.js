import express from 'express';
import * as db from '../db/sportpalyadb.js';

const router = express.Router();

// egy adott palyahoz tartpzo foglalasok lekerese
router.get('/foglalasokMegtekintese/:id', async (req, resp) => {
  try {
    const adatok = await db.selectFoglalasok(req.params.id);
    resp.send(adatok);
  } catch (err) {
    resp.status(500).send('Hiba lépett fel a lekérdezéskor.!');
  }
});

// egy adott palyahoz tartozo foglalasok lekerese
router.get('/foglalasokLekerese/:id', async (req, resp) => {
  const { id } = req.params;
  console.log(id);
  try {
    console.log('logout');
    const adatok = await db.foglalasokLekereseID(id);
    resp.send(adatok);
  } catch (err) {
    resp.status(500).send('Hiba lépett fel a lekérdezéskor.!');
    console.error(err);
  }
});

// egy adott foglalas vegrehajtasa
router.get('/lefoglalas/:id/:ora/:napSzam', async (req, resp) => {
  const { id } = req.params;
  const { ora } = req.params;
  const { napSzam } = req.params;

  const date = new Date();
  const n = date.getDay();
  const ev = date.getFullYear();
  const honap =  date.getMonth();
  const nap = date.getDate() + napSzam - n;
  const dateKezd = new Date(ev, honap, nap, ora, 0, 0);
  const dateVeg = new Date(ev, honap, nap, ora + 1, 0, 0);
  console.log(dateKezd);
  console.log(dateVeg);

  const { szemID } = resp.locals.tokenObj;
  const foglalas = {
    szemID,
    ID: id,
    kezdDatum: dateKezd,
    vegDatum: dateVeg,
  };
  try {
    await db.insertFoglalas(foglalas);
    resp.status(204).send();
  } catch (err) {
    resp.status(500).send('Hiba lépett fel a foglaláskor.!');
    console.error(err);
  }
});

// foglalas torlese az adatbazisbol
router.delete('/foglalasTorlese/:id', async (req, resp) => {
  try {
    await db.foglalasTorlese(req.params.id);
    resp.status(204).send();
  } catch (err) {
    resp.status(500).send('Hiba lépett fel a foglalás torlésekor.!');
    console.error(err);
  }
});

// felhasznalo torlese az adatbazisbol
router.delete('/felhasznaloTorlese/:id', async (req, resp) => {
  if (resp.locals.tokenObj.jogosultsag === 0) {
    try {
      await db.felhasznaloTorlese(req.params.id);
      resp.status(204).send();
    } catch (err) {
      resp.status(500).send('Hiba lépett fel a foglalás torlésekor.!');
      console.error(err);
    }
  } else {
    resp.status(401).send('Az adott felhasználónak nincs joga elvégezni a műveletet!');
  }
});

// varolista egy bejegyzesenek torlese az adatbazisbol
router.delete('/varolistaTorlese/:id', async (req, resp) => {
  if (resp.locals.tokenObj.jogosultsag === 0) {
    try {
      await db.varolistaTorlese(req.params.id);
      resp.status(204).send();
    } catch (err) {
      resp.status(500).send('Hiba lépett fel a várólista sorának torlésekor.!');
      console.error(err);
    }
  } else {
    resp.status(401).send('Az adott felhasználónak nincs joga elvégezni a műveletet!');
  }
});

// felhasznalo regisztraciojanak engedelyezese
router.delete('/felhasznaloEngedelyezese/:id', async (req, resp) => {
  if (resp.locals.tokenObj.jogosultsag === 0) {
    try {
      await db.varolistabolFelhasznaloba(req.params.id);
      resp.status(204).send();
    } catch (err) {
      resp.status(500).send('Hiba lépett fel a várólista sorából való átvitelkor.!');
      console.error(err);
    }
  } else {
    resp.status(401).send('Az adott felhasználónak nincs joga elvégezni a műveletet!');
  }
});

// egy megadott nevu kep torlese a rendszerbol
router.delete('/kepTorlese/:nev', async (req, resp) => {
  const szemID = await db.selectKepekSzemID(req.params.nev);
  if (szemID.szemID === resp.locals.tokenObj.szemID || resp.locals.tokenObj.jogosultsag === 0) {
    try {
      await db.kepTorlese(req.params.nev);
      console.log('Szioka');
      resp.status(204).send();
    } catch (err) {
      resp.status(500).send('Hiba lépett fel a kép torlésekor.!');
      console.error(err);
    }
  } else {
    resp.status(401).send('Az adott felhasználónak nincs joga elvégezni a műveletet!');
  }
});

export default router;
