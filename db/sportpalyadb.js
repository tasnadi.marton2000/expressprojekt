import util from 'util';
import mysql from 'mysql';

const pool = mysql.createPool({
  connectionLimit: 4,
  host: 'localhost',
  database: 'sportpalyak',
  port: 3306,
  user: 'root',
  password: '',
});

const queryPromise = util.promisify(pool.query).bind(pool);

export function createTableFelhasznalo() {
  return queryPromise(` CREATE TABLE IF NOT EXISTS felhasznalo(
      szemID INT AUTO_INCREMENT PRIMARY KEY,
      nev VARCHAR(30),
      felhasznalonev VARCHAR(50) NOT NULL UNIQUE, 
      jelszo VARCHAR(200), 
      jogosultsag INT ) `);
}

export function createTableVarolista() {
  return queryPromise(` CREATE TABLE IF NOT EXISTS varolista(
    varolistaID INT AUTO_INCREMENT PRIMARY KEY,
    nev VARCHAR(30),
    felhasznalonev VARCHAR(50), 
    jelszo VARCHAR(200) ) `);
}

export function createTablePalya() {
  return queryPromise(` CREATE TABLE IF NOT EXISTS palya(
    ID INT AUTO_INCREMENT PRIMARY KEY,
    szemID INT,
    tipus VARCHAR(30),
    oraber FLOAT,
    cim VARCHAR(100),
    leiras VARCHAR(1000), 
    CONSTRAINT FK_palya_szemID FOREIGN KEY(szemID) REFERENCES felhasznalo(szemID) )`);
}

export function createTableFoglalas() {
  return queryPromise(` CREATE TABLE IF NOT EXISTS foglalas(
      foglalasID INT AUTO_INCREMENT PRIMARY KEY,
      szemID INT,
      ID INT,
      kezdDatum DATETIME,
      vegDatum DATETIME,
      CONSTRAINT FK_foglalas_ID FOREIGN KEY(ID) REFERENCES palya(ID),
      CONSTRAINT FK_foglalas_szemID FOREIGN KEY(szemID) REFERENCES felhasznalo(szemID) )`);
}

export function createTableFenykep() {
  return queryPromise(` CREATE TABLE IF NOT EXISTS fenykep(
      ID INT, 
      kepnev VARCHAR(100),
      CONSTRAINT FK_fenykep_ID FOREIGN KEY(ID) REFERENCES palya(ID) )`);
}

export function insertPalya(palya) {
  const insert = 'INSERT INTO palya (tipus, oraber, cim, leiras, szemID) VALUES (?, ?, ?, ?, ?)';
  return queryPromise(insert, [palya.tipus, palya.oraber, palya.cim, palya.leiras, palya.szemID]);
}

export function insertKep(kep) {
  const insert = 'INSERT INTO fenykep (ID, kepnev) VALUES (?, ?)';
  return queryPromise(insert, [kep.ID, kep.kepnev]);
}

export function insertFoglalas(foglal) {
  const query = 'INSERT INTO foglalas (szemID, ID, kezdDatum, vegDatum) VALUES (?, ?, ?, ?)';
  return queryPromise(query, [foglal.szemID, foglal.ID, foglal.kezdDatum, foglal.vegDatum]);
}

export function insertVarolista(szemely) {
  const { jelszo } = szemely;
  const query = 'INSERT INTO varolista (nev, felhasznalonev, jelszo) VALUES (?, ?, ?)';
  return queryPromise(query, [szemely.nev, szemely.felhasznalonev, jelszo]);
}

export function palyakLekerese() {
  return queryPromise('SELECT * FROM palya');
}

export function selectKepID(id) {
  const query = `SELECT kepnev FROM fenykep WHERE ID = ${id} `;
  return queryPromise(query);
}

export async function selectPalyaID(id) {
  const query = await queryPromise(`SELECT * FROM palya WHERE ID = ${id} `);
  return query[0];
}

export async function selectPalyaSzemID(id) {
  const query = await queryPromise(`SELECT szemID FROM palya WHERE ID = ${id} `);
  return query[0];
}

export async function selectKepekSzemID(nev) {
  const query = await queryPromise(`SELECT szemID FROM fenykep JOIN palya ON fenykep.ID = palya.ID WHERE fenykep.kepnev = "${nev}" `);
  return query[0];
}

export function selectFoglalasok(id) {
  const select = 'SELECT foglalas.foglalasID, foglalas.kezdDatum, foglalas.vegDatum, felhasznalo.nev, felhasznalo.szemID FROM foglalas';
  const query = `${select} JOIN felhasznalo ON foglalas.szemID = felhasznalo.szemID WHERE foglalas.ID = ${id} `;
  return queryPromise(query);
}

export function countFelhasznalonev(felhasznalonev) {
  const query = ` SELECT COUNT(szemID) AS darab FROM felhasznalo WHERE felhasznalonev = "${felhasznalonev}" `;
  return queryPromise(query);
}

export function countVarolista(felhasznalonev) {
  const query = ` SELECT COUNT(varolistaID) AS darab FROM varolista WHERE felhasznalonev = "${felhasznalonev}" `;
  return queryPromise(query);
}

export async function selectSzemID(felhasznalonev) {
  const query = await queryPromise(` SELECT szemID FROM felhasznalo WHERE felhasznalonev = "${felhasznalonev}" `);
  return query[0].szemID;
}

export async function selectKepSzemID(id) {
  const query = await queryPromise(` SELECT szemID FROM palya WHERE ID = ${id} `);
  return query[0].szemID;
}

export function selectAdatok(felhasznalonev) {
  const query = ` SELECT * FROM felhasznalo WHERE felhasznalonev = "${felhasznalonev}" `;
  return queryPromise(query);
}

export function foglalasTorlese(id) {
  const query = ` DELETE FROM foglalas WHERE foglalasID = ${id} `;
  return queryPromise(query);
}

export function deleteFenykep() {
  const query = 'DELETE FROM fenykep';
  return queryPromise(query);
}

export function selectNevek() {
  const query = ' SELECT * FROM felhasznalo';
  return queryPromise(query);
}

export function selectPalyak() {
  const query = ' SELECT ID FROM palya';
  return queryPromise(query);
}

export function foglalasokIdopontja(id) {
  const query = `SELECT * FROM foglalas WHERE ID = ${id}`;
  return queryPromise(query);
}

export function insertFelhasznalo(szemely) {
  const { jelszo } = szemely;
  const query = 'INSERT INTO felhasznalo (nev, felhasznalonev, jelszo, jogosultsag) VALUES (?, ?, ?, ?)';
  const parameterlista = [szemely.nev, szemely.felhasznalonev, jelszo, szemely.jogosultsag];
  return queryPromise(query, parameterlista);
}

export function palyaKeresese(szo) {
  const query = `SELECT * FROM palya WHERE tipus LIKE '%${szo}%'`;
  return queryPromise(query);
}

export function foglalasokLekerese() {
  const query1 = 'SELECT * FROM foglalas JOIN felhasznalo ON felhasznalo.szemID = foglalas.szemID';
  const query2 = 'JOIN palya ON foglalas.ID = palya.ID';
  const query = ` ${query1} ${query2}`;
  return queryPromise(query);
}

export function felhasznalokLekerese() {
  const query = 'SELECT * FROM felhasznalo';
  return queryPromise(query);
}

export function felhasznaloTorlese(id) {
  let query = ` DELETE FROM palya WHERE szemID = ${id}`;
  queryPromise(query);
  query = ` DELETE FROM foglalas WHERE szemID = ${id}`;
  queryPromise(query);
  query = `DELETE FROM felhasznalo WHERE szemID = ${id}`;
  return queryPromise(query);
}

export function kepTorlese(nev) {
  const query = ` DELETE FROM fenykep WHERE kepnev = "${nev}"`;
  return queryPromise(query);
}

export async function szemelyAdatok(id) {
  const query = await queryPromise(` SELECT * FROM felhasznalo WHERE szemID  = ${id}`);
  return query[0];
}

export function adatokatModosit(szemID, teljesNev, felhasznaloNev, ujJelszo) {
  const jelszo = ujJelszo;
  const query = `UPDATE felhasznalo SET nev = ?, felhasznalonev = ?, jelszo = ?, jogosultsag = 1 WHERE szemID = ${szemID}`;
  return queryPromise(query, [teljesNev, felhasznaloNev, jelszo]);
}

export function varolistaTorlese(id) {
  const query = `DELETE FROM varolista WHERE varolistaID = ${id}`;
  return queryPromise(query);
}

export async function varolistabol(id) {
  const query = queryPromise(`SELECT * FROM varolista WHERE varolistaID = ${id}`);
  return query;
}

export async function varolistabolFelhasznaloba(id) {
  const felhasznalo = await varolistabol(id);
  await varolistaTorlese(id);
  const insert = 'INSERT INTO felhasznalo (nev, felhasznalonev, jelszo, jogosultsag) VALUES (?, ?, ?, ?)';
  const lista = [felhasznalo[0].nev, felhasznalo[0].felhasznalonev, felhasznalo[0].jelszo, 1];
  return queryPromise(insert, lista);
}

export function varolistaLekerese() {
  const query = 'SELECT * FROM varolista ';
  return queryPromise(query);
}

export function foglalasokLekereseID(id) {
  const query = `SELECT * FROM foglalas WHERE ID = ${id} `;
  return queryPromise(query);
}

export function palyaSzurese(datum) {
  const vegsoDatum = datum.toISOString();
  const query = ` SELECT DISTINCT(ID) FROM foglalas WHERE "${vegsoDatum}" < kezdDatum OR "${vegsoDatum}" > vegDatum`;
  const query1 = ' UNION SELECT DISTINCT(ID) from palya WHERE ID NOT IN ( SELECT DISTINCT(ID) FROM foglalas ) ';
  return queryPromise(` ${query} ${query1}`);
}

(async () => {
  try {
    await createTableFelhasznalo();
    await createTablePalya();
    await createTableFoglalas();
    await createTableFenykep();
    await createTableVarolista();
    /* insertFelhasznalo({
      nev: 'admin',
      felhasznalonev: 'admin',
      jelszo: 'admin',
      jogosultsag: 0,
    });
    insertFelhasznalo({
      nev: 'Balogh Levente',
      felhasznalonev: 'levicike',
      jelszo: 'capa',
      jogosultsag: 1,
    });
    insertFelhasznalo({
      nev: 'Tasnadi Marton',
      felhasznalonev: 'marcikaist',
      jelszo: 'marcikaist2000',
      jogosultsag: 1,
    });
    await insertPalya({
      tipus: 'foci',
      oraber: 75,
      cim: 'Bulevardul Muncii, 25. C4',
      leiras: 'Mufuves palya legkondival.',
      szemID: 1,
    }); */
    await deleteFenykep();
  } catch (err) {
    console.log(`Error tortent: ${err}`);
  }
})();
