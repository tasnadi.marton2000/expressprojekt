import express from 'express';
import morgan from 'morgan';
import eformidable from 'express-formidable';
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import cookieParser from 'cookie-parser';
import keresekRoutes from './router/keresek.js';
import apiRouter from './router/api.js';
import authRouter from './authentication/auth.js';
import { tokenDekodolasa } from './authentication/middleware.js';
import regisztraciosRouter from './regisztracio/regisztracio.js';

const app = express();
const port = 8080;
const uploadDir = join(process.cwd(), 'static/uploadDir');
const kepek = join(process.cwd(), 'static/kepek');

app.use(morgan('tiny'));
app.use(cookieParser());
app.use(tokenDekodolasa);
app.use(express.static('static'));
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(keresekRoutes);
app.use('/api', apiRouter);
app.use('/authentication', authRouter);
app.use('/regisztracio', regisztraciosRouter);

if (!existsSync(uploadDir)) {
  mkdirSync(uploadDir);
}

app.use(eformidable({ uploadDir }));
app.use(eformidable({ kepek }));

app.listen(port, () => {
  console.log(`A szerver a ${port} porton hallgat.`);
});
